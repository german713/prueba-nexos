INSERT INTO familias (nombre) VALUES('Perez Martinez');


INSERT INTO roles(nombre) VALUES('esposo');
INSERT INTO roles(nombre) VALUES('esposa');

INSERT INTO roles(nombre) VALUES('hijo');
INSERT INTO roles(nombre) VALUES('hija');

INSERT INTO personas (nombre, apellido1, apellido2, edad, id_rol, id_familia) VALUES ('Carlos', 'Perez', 'S', '40', '2', '1');
INSERT INTO personas (nombre, apellido1, apellido2, edad, id_rol, id_familia) VALUES ('Valentina', 'Martinez', 'P', '40', '1', '1');
INSERT INTO personas (nombre, apellido1, apellido2, edad, id_rol, id_familia) VALUES ('Juan', 'Perez', 'Martinez', '10', '3', '1');