/**
 * 
 */
package com.pruebatecnica.familias.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.pruebatecnica.commons.entitys.Persona;
import com.pruebatecnica.familias.service.IFamiliaService;
import com.pruebatecnica.familias.service.IPersonaService;

/**
 * @author German Uchamocha E.
 *
 */
@RestController
public class PersonaController {
	
	@Autowired
	private IPersonaService personaService;
	
	@Autowired
	private IFamiliaService familiaService;
	
	
	@GetMapping("/persona/{id}")
	public Persona getPersona(@PathVariable int id) {
		return personaService.findById(id);
	}

	@GetMapping("/persona")
	public List<Persona> getListPersonas() {
		
		List<Persona> personas=personaService.findAll();
		
		personas.stream().map(p->{
			p.setRolFamilar(p.getRol().getNombre());			
			return p;
			
		}).collect(Collectors.toList());
		
		return personas;
	}
	
	@PutMapping("/persona/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Persona editarPersona(@RequestBody Persona persona, @PathVariable int id) throws Exception {		
		Persona personaEditar= personaService.findById(id);		
		personaEditar.setApellido1(persona.getApellido1());
		personaEditar.setApellido2(persona.getApellido2());
		personaEditar.setNombre(persona.getNombre());
		personaEditar.setEdad(persona.getEdad());				
		return personaService.save(personaEditar);							
	}
	
	@DeleteMapping("/persona/{id}")
    public void eliminaPersona(@PathVariable int id) {		
		Persona personaEliminar= personaService.findById(id);		
		familiaService.deleteById(personaEliminar.getFamilia().getId());
	}				

}
