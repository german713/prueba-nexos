/**
 * 
 */
package com.pruebatecnica.familias.controllers;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.pruebatecnica.commons.dtos.NucleoFamiliar;
import com.pruebatecnica.commons.dtos.PersonaDTO;
import com.pruebatecnica.commons.entitys.Familia;
import com.pruebatecnica.commons.entitys.Persona;
import com.pruebatecnica.commons.entitys.Rol;
import com.pruebatecnica.familias.service.IFamiliaService;
import com.pruebatecnica.familias.service.IPersonaService;
import com.pruebatecnica.familias.service.IRolService;
import com.pruebatecnica.familias.utils.Utilidades;
import brave.Tracer;

/**
 * @author German Uchamocha E.
 *
 */
@RestController
public class NucleoFamiliarController {

	@Autowired
	private IPersonaService personaService;

	@Autowired
	private IFamiliaService familiaService;

	@Autowired
	private IRolService rolService;

	@Autowired
	private Tracer tracer;

	@PostMapping("/familiar")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Familia> crearNucleoFamiliar(@RequestBody PersonaDTO personaDto) throws Exception {				

		try {
		Persona persona = new Persona();
		persona.setNombre(personaDto.getNombre());
		persona.setApellido1(personaDto.getApellido1());
		persona.setApellido2(personaDto.getApellido2());
		persona.setEdad(personaDto.getEdad());
		persona.setRolFamilar(personaDto.getRolFamiliar());

		Rol rol = rolService.findByNombre(personaDto.getRolFamiliar());
		persona.setRol(rol);

		List<Persona> personas = personaDto.getNucleoFamiliar().getPersonas();
		personas.add(persona);

		List<String> listRoles = personas.stream().map(v -> v.getRolFamilar().toLowerCase()).collect(Collectors.toList());

		int countEsposa = checkPadreMadre(listRoles, "esposa");
		int countEsposo = checkPadreMadre(listRoles, "esposo");

		if (!(countEsposa == 1 && countEsposo == 1)) {
			String mensajeError = "No se puede agregar el registro, el nucleo familiar nesecita de un esposo y una esposa";
			tracer.currentSpan().tag("error.mensaje", mensajeError);
			throw new Exception(mensajeError);
		}

		Familia nuevaFamilia = new Familia(); //Se crea la familia, para poder relacionar a las personas a un nucleo familiar

		Map<String, Persona> cabezasFamilia = personas.stream()
				.filter(p -> p.getRolFamilar().toLowerCase().equals("esposo") || p.getRolFamilar().toLowerCase().equals("esposa"))
				.map(p->{
					p.setRolFamilar(p.getRolFamilar().toLowerCase());
					return p;
				})
				.collect(Collectors.toMap(Persona::getRolFamilar, p -> p));

		Persona esposa = cabezasFamilia.get("esposa");
		Persona esposo = cabezasFamilia.get("esposo");
        //el nombre de la familia se compone del primer apellido del esposo y del primer apellido de la esposa
		nuevaFamilia.setNombre(esposo.getApellido1() + " " + esposa.getApellido1());

		familiaService.save(nuevaFamilia);

		for (Persona p1 : personas) {
			p1.setRol(rolService.findByNombre(p1.getRolFamilar().toLowerCase()));
			p1.setFamilia(nuevaFamilia);
			personaService.save(p1);
		}

		String payLoad = Utilidades.parametrosAuditoria(personaDto);
		String response = Utilidades.parametrosAuditoria(nuevaFamilia);

		tracer.currentSpan().tag("parametros.entrada", payLoad);
		tracer.currentSpan().tag("parametros.salida", response);
		return new ResponseEntity<Familia>(nuevaFamilia,HttpStatus.OK);
		}catch (Exception e) {
			String mensajeError = e.getMessage();
			tracer.currentSpan().tag("error.mensaje", mensajeError);
			throw new Exception(mensajeError);
		}

	}

	@GetMapping("/familiar/{id}")
	public PersonaDTO getNucleoFamiliarPersona(@PathVariable int id) {

		PersonaDTO personaDTO = new PersonaDTO();
		NucleoFamiliar nucleo = new NucleoFamiliar();

		Persona persona = personaService.findById(id);

		if (persona != null) {
			int idf = persona.getFamilia().getId();
			personaDTO.setNombre(persona.getNombre());
			personaDTO.setApellido1(persona.getApellido1());
			personaDTO.setApellido2(persona.getApellido2());
			personaDTO.setRolFamiliar(persona.getRol().getNombre());
			personaDTO.setEdad(persona.getEdad());

			List<Persona> l1 = familiaService.findById(idf).getPersonas().stream().map(p -> {
				p.setRolFamilar(p.getRol().getNombre());
				return p;
			}).filter(v -> v.getId() != id).collect(Collectors.toList());

			nucleo.setPersonas(l1);
			personaDTO.setNucleoFamiliar(nucleo);
		}

		String response = Utilidades.parametrosAuditoria(personaDTO);
		tracer.currentSpan().tag("parametros.salida", response);
		return personaDTO;
	}

	private int checkPadreMadre(List<String> listRoles, String rol) {
		return (int) listRoles.stream().filter(v -> v.equals(rol)).count();
	}

}
