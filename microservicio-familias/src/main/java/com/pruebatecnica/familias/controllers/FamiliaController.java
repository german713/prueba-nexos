/**
 * 
 */
package com.pruebatecnica.familias.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pruebatecnica.commons.entitys.Familia;
import com.pruebatecnica.commons.entitys.Rol;
import com.pruebatecnica.familias.service.IFamiliaService;
import com.pruebatecnica.familias.service.IRolService;

/**
 * @author German Uchamocha E.
 *
 */

@RestController
public class FamiliaController {

	@Autowired
	private IFamiliaService familiaService;

	@Autowired
	private IRolService rolService;

	@GetMapping("/familias")
	public List<Familia> listar() {
		return familiaService.findAll();
	}

	@GetMapping("/rol")
	public List<Rol> getRol() {
		return rolService.findAll();
	}

}
