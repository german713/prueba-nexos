package com.pruebatecnica.familias.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.pruebatecnica.commons.entitys.Rol;

/**
 * @author German Uchamocha E.
 *
 */
public interface RolDao extends CrudRepository<Rol, Integer> {
	
	
	public Optional<Rol> findByNombre(String nombre);
	
	
	

}
