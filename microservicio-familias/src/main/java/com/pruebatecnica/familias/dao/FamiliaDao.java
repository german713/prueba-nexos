package com.pruebatecnica.familias.dao;

import org.springframework.data.repository.CrudRepository;

import com.pruebatecnica.commons.entitys.Familia;

/**
 * @author German Uchamocha E.
 *
 */
public interface FamiliaDao extends CrudRepository<Familia, Integer> {

}
