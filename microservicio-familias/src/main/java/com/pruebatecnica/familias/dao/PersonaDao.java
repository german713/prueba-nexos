package com.pruebatecnica.familias.dao;

import org.springframework.data.repository.CrudRepository;

import com.pruebatecnica.commons.entitys.Persona;

/**
 * @author German Uchamocha E.
 *
 */
public interface PersonaDao extends CrudRepository<Persona, Integer>  {
	
	

}
