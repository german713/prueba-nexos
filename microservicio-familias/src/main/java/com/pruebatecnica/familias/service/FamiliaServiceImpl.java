package com.pruebatecnica.familias.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pruebatecnica.familias.dao.FamiliaDao;
import com.pruebatecnica.commons.entitys.Familia;

/**
 * @author German Uchamocha E.
 *
 */

@Service
public class FamiliaServiceImpl implements IFamiliaService {

	@Autowired
	private FamiliaDao familiaDao;

	@Override
	@Transactional(readOnly = true)
	public Familia findById(int id) {
		return familiaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Familia> findAll() {

		return (List<Familia>) familiaDao.findAll();
	}

	@Override
	@Transactional
	public Familia save(Familia familia) {
		return familiaDao.save(familia);
	}

	
	@Override
	@Transactional
	public void deleteById(int id) {
		familiaDao.deleteById(id);
	}

}
