package com.pruebatecnica.familias.service;

import java.util.List;

import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pruebatecnica.familias.dao.PersonaDao;
import com.pruebatecnica.familias.utils.Utilidades;
import com.pruebatecnica.commons.entitys.Persona;

/**
 * @author German Uchamocha E.
 *
 */
@Service
public class PersonaServiceImpl implements IPersonaService {
	
	@Autowired
	private static ValidatorFactory validatorFactory;

	@Autowired
	private static Validator validator;
	
	
	@Autowired
	private PersonaDao personaDao;

	@Override
	@Transactional(readOnly = true)
	public List<Persona> findAll() {
		return (List<Persona>)personaDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Persona findById(int id) {
		return personaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional	
	public Persona save(Persona persona) throws Exception {
		
		Utilidades.validaConstraintViolations(persona, validatorFactory, validator);
		
		return personaDao.save(persona);
	}

	@Override
	@Transactional
	public void deleteById(int id) {
		personaDao.deleteById(id);		
	}
}
