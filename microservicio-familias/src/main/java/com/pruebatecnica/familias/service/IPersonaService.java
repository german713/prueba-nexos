package com.pruebatecnica.familias.service;

import java.util.List;

import com.pruebatecnica.commons.entitys.Persona;

/**
 * @author German Uchamocha E.
 *
 */

public interface IPersonaService {
	
	
	public List<Persona> findAll();
	public Persona findById(int id);
	public Persona save(Persona persona) throws Exception;
	public void deleteById(int id);
	
	
	
	

}
