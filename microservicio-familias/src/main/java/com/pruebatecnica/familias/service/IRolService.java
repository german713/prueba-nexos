package com.pruebatecnica.familias.service;

import java.util.List;

import com.pruebatecnica.commons.entitys.Rol;

/**
 * @author German Uchamocha E.
 *
 */

public interface IRolService {
	public Rol findById(int id);
	
	public Rol findByNombre(String nombre);
	
	
	List<Rol> findAll();
}
