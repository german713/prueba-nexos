package com.pruebatecnica.familias.service;

import java.util.List;

import com.pruebatecnica.commons.entitys.Familia;

/**
 * @author German Uchamocha E.
 *
 */

public interface IFamiliaService {
	
	public Familia findById(int id);
	public List<Familia> findAll();	
	public Familia save(Familia familia);
	void deleteById(int id);
	

}
