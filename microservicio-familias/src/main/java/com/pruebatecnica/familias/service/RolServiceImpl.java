package com.pruebatecnica.familias.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pruebatecnica.familias.dao.RolDao;
import com.pruebatecnica.commons.entitys.Rol;

/**
 * @author German Uchamocha E.
 *
 */
@Service
public class RolServiceImpl implements IRolService {
	
	@Autowired
	private RolDao rolDao;

	@Override
	@Transactional(readOnly = true)
	public Rol findById(int id) {
		return rolDao.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Rol> findAll() {
		return (List<Rol>)rolDao.findAll();
	}

	@Override
	public Rol findByNombre(String nombre) {		
		return rolDao.findByNombre(nombre).orElse(null);
	}

}
