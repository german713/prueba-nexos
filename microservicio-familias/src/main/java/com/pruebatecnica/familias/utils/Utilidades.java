/**
 * 
 */
package com.pruebatecnica.familias.utils;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author German Uchamocha E.
 *
 */
public class Utilidades {
	
	@Autowired
	private static ValidatorFactory validatorFactory;

	@Autowired
	private static Validator validator;
	
	public static <T> String parametrosAuditoria(T parametro) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(parametro);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return "";
		}
	}
	


	
	//Valida que cualquier entidad que va ser persistida cumpla con los constraint anotados en las clases entitys
	
	public static <T> void validaConstraintViolations(T objeto, ValidatorFactory validatorFactory,Validator validator) throws Exception{
		
		validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
		Set<ConstraintViolation<T>> constraintViolations = validator.validate(objeto);
		
		StringBuilder mensajeNovacios= new StringBuilder("Los siguientes campos no pueden estar vacios o nulos: ");
		if (constraintViolations.size() > 0) {
			for (ConstraintViolation<T> violation : constraintViolations) {				
				mensajeNovacios.append(violation.getMessage()+", ");				
			}			
			int ultimaComa=mensajeNovacios.lastIndexOf(",");
			mensajeNovacios.replace(ultimaComa, ultimaComa+1, "");
			throw new Exception(mensajeNovacios.toString().trim());
		} 			
	}
	
}
