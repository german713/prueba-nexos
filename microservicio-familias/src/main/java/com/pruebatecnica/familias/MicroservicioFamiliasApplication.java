package com.pruebatecnica.familias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@EntityScan({"com.pruebatecnica.commons.entitys"})
@SpringBootApplication
public class MicroservicioFamiliasApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroservicioFamiliasApplication.class, args);
	}

}
