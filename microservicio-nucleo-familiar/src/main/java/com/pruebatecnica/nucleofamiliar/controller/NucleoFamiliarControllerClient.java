/**
 * 
 */
package com.pruebatecnica.nucleofamiliar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pruebatecnica.commons.dtos.PersonaDTO;
import com.pruebatecnica.commons.entitys.Familia;
import com.pruebatecnica.nucleofamiliar.service.INucleoFamiliarService;

/**
 * @author German Uchamocha E.
 *
 */

@RestController
public class NucleoFamiliarControllerClient {
	
	
	@Autowired
	private INucleoFamiliarService iNucleoFamiliarService;
	
	
	@GetMapping("/familiar/{id}")	
	public PersonaDTO getNucleoFamiliar(@PathVariable int id) {		
		return iNucleoFamiliarService.getNucleoFamiliar(id);
	}
	
	@PostMapping("/familiar")
	public  ResponseEntity<Familia> crearNucleoFamiliar(@RequestBody PersonaDTO persona) {		
		
		return iNucleoFamiliarService.crearNucleoFamilar(persona);
		
		
	}
	
	

}
