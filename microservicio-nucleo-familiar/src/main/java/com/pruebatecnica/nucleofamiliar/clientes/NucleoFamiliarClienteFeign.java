/**
 * 
 */
package com.pruebatecnica.nucleofamiliar.clientes;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.pruebatecnica.commons.dtos.PersonaDTO;
import com.pruebatecnica.commons.entitys.Familia;

/**
 * @author German Uchamocha E.
 *
 */
@FeignClient(name="microservicio-familias",url="localhost:8001")            
public interface NucleoFamiliarClienteFeign {
	
	@PostMapping("/familiar")	
	public ResponseEntity<Familia> crearNucleoFamiliar(@RequestBody PersonaDTO personaDto);		
	
	
	@GetMapping("/familiar/{id}")
	public PersonaDTO getNucleoFamiliarPersona(@PathVariable int id);
	
	

}
