/**
 * 
 */
package com.pruebatecnica.nucleofamiliar.service;

import org.springframework.http.ResponseEntity;

import com.pruebatecnica.commons.dtos.PersonaDTO;
import com.pruebatecnica.commons.entitys.Familia;

/**
 * @author German Uchamocha E.
 *
 */
public interface INucleoFamiliarService {
	
	
	public  ResponseEntity<Familia> crearNucleoFamilar(PersonaDTO personaDto);
	
	public PersonaDTO getNucleoFamiliar(int idPersona);
	
	

}
