/**
 * 
 */
package com.pruebatecnica.nucleofamiliar.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.pruebatecnica.commons.dtos.PersonaDTO;
import com.pruebatecnica.commons.entitys.Familia;
import com.pruebatecnica.nucleofamiliar.clientes.NucleoFamiliarClienteFeign;

/**
 * @author German Uchamocha E.
 *
 */
@Service
public class NucleoFamiliarServiceImpl implements INucleoFamiliarService {
	
	@Autowired
	private NucleoFamiliarClienteFeign nucleoFamilarFeign;

	@Override
	public ResponseEntity<Familia> crearNucleoFamilar(PersonaDTO personaDto) {
		return nucleoFamilarFeign.crearNucleoFamiliar(personaDto);
	}

	@Override
	public PersonaDTO getNucleoFamiliar(int idPersona) {
		return nucleoFamilarFeign.getNucleoFamiliarPersona(idPersona);
	}

	
}
