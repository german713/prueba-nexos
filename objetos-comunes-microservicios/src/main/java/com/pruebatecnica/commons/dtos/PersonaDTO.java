package com.pruebatecnica.commons.dtos;

import java.io.Serializable;

/**
 * @author German Uchamocha E.
 *
 */
public class PersonaDTO implements Serializable {			
	
	private static final long serialVersionUID = 1L;
	private String nombre;
	private String apellido1;
	private String apellido2;		
	private int edad;
	private String rolFamiliar;
	private int idRol;
	private NucleoFamiliar nucleoFamiliar;	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getApellido1() {
		return apellido1;
	}
	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}
	public String getApellido2() {
		return apellido2;
	}
	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}		
	public String getRolFamiliar() {
		return rolFamiliar;
	}
	public void setRolFamiliar(String rolFamiliar) {
		this.rolFamiliar = rolFamiliar;
	}
	public int getIdRol() {
		return idRol;
	}
	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}
	public NucleoFamiliar getNucleoFamiliar() {
		return nucleoFamiliar;
	}
	public void setNucleoFamiliar(NucleoFamiliar nucleoFamiliar) {
		this.nucleoFamiliar = nucleoFamiliar;
	}

}
