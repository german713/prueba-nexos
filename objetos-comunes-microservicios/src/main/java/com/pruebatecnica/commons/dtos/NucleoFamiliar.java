package com.pruebatecnica.commons.dtos;

import java.util.List;

import com.pruebatecnica.commons.entitys.Persona;


/**
 * @author German Uchamocha E.
 *
 */
public class NucleoFamiliar {		
	
	private List<Persona> personas;

	public List<Persona> getPersonas() {
		return personas;
	}

	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}
	
	

}
