/**
 * 
 */
package com.pruebatecnica.commons.entitys;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author German Uchamocha E.
 *
 */

@Entity
@Table(name="personas")
public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Basic(optional = false)
	@NotEmpty(message = "Nombre")
	private String nombre;	
	
	@Basic(optional = false)
	@NotEmpty(message = "Apellido1")

	private String apellido1;

	private String apellido2;

	private int edad;
	
	@Transient	
	private String rolFamilar;	
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_familia",referencedColumnName = "id")
	@JsonIgnore
	private Familia familia;
	

	@ManyToOne( fetch = FetchType.LAZY)
	@JoinColumn(name="id_rol")
	@JsonIgnore
	private Rol rol;

	public Persona() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApellido1() {
		return this.apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return this.apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public int getEdad() {
		return this.edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Familia getFamilia() {
		return this.familia;
	}

	public void setFamilia(Familia familia) {
		this.familia = familia;
	}

	public Rol getRol() {
		return this.rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public String getRolFamilar() {
		return rolFamilar;
	}

	public void setRolFamilar(String rolFamilar) {
		this.rolFamilar = rolFamilar;
	}

}

