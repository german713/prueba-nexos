El proyecto esta conformado por:

El microservicio-familias, que contiene los DAO y el acceso a la base de datos y los webservices CRUD

El microservicio-nucleo-familiar, que es cliente del microservicio-familias y expone 2 webservices, el de crear nucleo familiar y obtener nucleo familiar

El servidor eureka que registra a los 2 anteriores microservicios y ademas al microservicio-zuul-server, este sirve como gateway o punto de enlace para conectar a los demas microservicios

Un proyecto llamado objetos-comunes-microservicios, que empaqueta los objetos comunes, que se usan para intercambiar informacion entre los demas microservicios

Se usa zipkin y rabbit como brooker, esto evita que los microservicios envien las trazas como http, las va enviar mediante el brooker, asi se evitan bloqueos

Link documentacion webservices

https://documenter.getpostman.com/view/7959055/SztD5n9M
